CREATE role transaction_manager;
ALTER role transaction_manager PASSWORD 'transaction_manager';

CREATE DATABASE transaction_manager_db WITH OWNER transaction_manager;
Alter user transaction_manager with superuser login;

--

CREATE SCHEMA operations;
ALTER SCHEMA operations OWNER TO transaction_manager;

CREATE SEQUENCE operations.client_id_seq;
CREATE TABLE operations.client
(
    id BIGINT DEFAULT nextval('operations.client_id_seq') NOT NULL  PRIMARY KEY,
    inn VARCHAR(10),
    first_name VARCHAR(50),
    middle_name VARCHAR(50),
    last_name VARCHAR (50)
);

CREATE SEQUENCE operations.transaction_id_seq;
CREATE TABLE operations.transaction
(
    id BIGINT DEFAULT nextval('operations.transaction_id_seq') NOT NULL PRIMARY KEY,
    place VARCHAR(100),
    amount DOUBLE PRECISION NOT NULL DEFAULT 0,
    card VARCHAR (16),
    currency VARCHAR (5),
    transaction_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    client_id BIGINT
);