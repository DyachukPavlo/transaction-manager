package com.transactionmanager.services;

import com.transactionmanager.domain.Client;
import com.transactionmanager.repo.ClientRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientSrv {
    private final ClientRepo clientRepo;

    public ClientSrv(ClientRepo clientRepo) {
        this.clientRepo = clientRepo;
    }

    public List<Client> findAll(){
        return clientRepo.findAll();
    }
}
