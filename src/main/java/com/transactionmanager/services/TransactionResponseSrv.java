package com.transactionmanager.services;

import com.transactionmanager.domain.TransactionsResponse;
import org.springframework.stereotype.Service;

@Service
public class TransactionResponseSrv {
    private final TransactionSrv transactionSrv;

    public TransactionResponseSrv(TransactionSrv transactionSrv) {
        this.transactionSrv = transactionSrv;
    }

    public void storeResponse(TransactionsResponse transactionsResponse){
        transactionSrv.save(transactionsResponse.getTransactions().getItems());
    }
}
