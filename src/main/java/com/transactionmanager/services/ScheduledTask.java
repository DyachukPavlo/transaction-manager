package com.transactionmanager.services;

import com.transactionmanager.domain.TransactionsResponse;
import com.transactionmanager.utils.FileParams;
import com.transactionmanager.utils.FileBroker;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class ScheduledTask {
    private final FileParams fileParams;
    private final TransactionSrv transactionSrv;

    public ScheduledTask(FileParams fileParams, TransactionSrv transactionSrv) {
        this.fileParams = fileParams;
        this.transactionSrv = transactionSrv;
    }

    @Scheduled(fixedDelay = 60_000L)
    private void doTask() {
        try {
            FileBroker<TransactionsResponse> fileBroker = new FileBroker<>(fileParams, TransactionsResponse.class);
            ExecutorService threadPool = Executors.newFixedThreadPool(4);
            threadPool.execute(new FileProducer<>(fileBroker));
            threadPool.execute(new FileConsumer(fileBroker, transactionSrv));
            threadPool.execute(new FileConsumer(fileBroker, transactionSrv));
            threadPool.execute(new FileConsumer(fileBroker, transactionSrv));
            threadPool.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
