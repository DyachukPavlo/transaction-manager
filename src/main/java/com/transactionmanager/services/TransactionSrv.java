package com.transactionmanager.services;

import com.transactionmanager.domain.Transaction;
import com.transactionmanager.repo.TransactionRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionSrv {
    private final TransactionRepo transactionRepo;

    public TransactionSrv(TransactionRepo transactionRepo) {
        this.transactionRepo = transactionRepo;
    }

    public Transaction save(Transaction transaction){
        return transactionRepo.save(transaction);
    }

    public List<Transaction> save(List<Transaction> transactions){
        return transactionRepo.saveAll(transactions);
    }

    public List<Transaction> findAll(){
        return transactionRepo.findAll();
    }
}
