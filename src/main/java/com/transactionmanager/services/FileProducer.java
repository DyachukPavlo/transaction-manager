package com.transactionmanager.services;

import com.transactionmanager.utils.FileBroker;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.WildcardFileFilter;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Thread.sleep;

public class FileProducer<T> implements Runnable{

    private FileBroker<T> fileBroker;

    public FileProducer(FileBroker<T> fileBroker) {
        this.fileBroker = fileBroker;
    }

    @Override
    public void run() {
        File folder = new File(Paths.get(fileBroker.getFileParams().getFrom()).toUri());
        FileFilter fileFilter = new WildcardFileFilter(fileBroker.getFileParams().getMask(), IOCase.INSENSITIVE);
        while (true){
            File[] listOfFiles = folder.listFiles(fileFilter);
            if (listOfFiles != null && listOfFiles.length>0){
                List<File> files = Arrays.asList(listOfFiles);
                files.forEach(item->{
                    fileBroker.put(move(Paths.get(fileBroker.getFileParams().getInProcess(),item.getName()), item));
                });
            }
            else {
                fileBroker.setProduced(true);
                break;
            }
        }
    }

    private File move(Path path, File file) {
        try {
            File newFile = new File(path.toUri());
            FileUtils.copyFile(file, newFile);
            file.delete();
            return newFile;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
