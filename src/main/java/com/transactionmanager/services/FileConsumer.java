package com.transactionmanager.services;

import com.transactionmanager.domain.TransactionsResponse;
import com.transactionmanager.utils.FileBroker;
import com.transactionmanager.utils.XmlParser;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.Thread.sleep;

public class FileConsumer implements Runnable {
    private final FileBroker<TransactionsResponse> fileBroker;
    private final TransactionSrv transactionSrv;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public FileConsumer(FileBroker<TransactionsResponse> fileBroker, TransactionSrv transactionSrv) {
        this.fileBroker = fileBroker;
        this.transactionSrv = transactionSrv;
    }

    @Override
    public void run() {
        while (true) {
            File data = fileBroker.get();
            if (data == null && fileBroker.isProduced() && fileBroker.getQueue().size() == 0) {
                break;
            } else {
                if (data != null){
                    process(data);
                }
            }
        }
    }

    private void process(File file){
        try {
            InputStream is = FileUtils.openInputStream(file);
            TransactionsResponse transactionsResponse = XmlParser.convert(is, fileBroker.getType());
            if (transactionsResponse == null){
                logger.error("File reading error");
                move(Paths.get(fileBroker.getFileParams().getError(),file.getName()), file);
            }
            move(Paths.get(fileBroker.getFileParams().getProcessed(),file.getName()), file);
            transactionSrv.save(transactionsResponse.getTransactions().getItems());
        } catch (IOException | JAXBException | SOAPException e) {
            e.printStackTrace();
            move(Paths.get(fileBroker.getFileParams().getError(),file.getName()), file);
        }
    }

    private void move(Path path, File file) {
        try {
            FileUtils.copyFile(file, new File(path.toUri()));
            file.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
