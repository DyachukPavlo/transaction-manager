package com.transactionmanager.config;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.persistence.EntityManagerFactory;

@Configuration
@EnableConfigurationProperties
@EnableJpaRepositories(basePackages = "com.transactionmanager")
public class Datasource {

    @Autowired
    private JPA jpaConfig;

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public javax.sql.DataSource dataSource() {
        return DataSourceBuilder.create().type(DataSource.class).build();
    }

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManager(JpaProperties jpaProperties) {
        EntityManagerFactoryBuilder builder = jpaConfig.createEntityManagerFactoryBuilder(jpaProperties);
        return builder
                .dataSource(dataSource())
                .packages("com.transactionmanager")
                .persistenceUnit("entityManagerFactoryBean")
                .build();
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManager) {
        return new JpaTransactionManager(entityManager);
    }
}