package com.transactionmanager.controllers;

import com.transactionmanager.domain.TransactionsResponse;
import com.transactionmanager.services.TransactionResponseSrv;
import com.transactionmanager.utils.XmlParser;
import com.transactionmanager.utils.Response;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@RestController
@RequestMapping
public class TransactionCtrl {
    private final TransactionResponseSrv transactionResponseSrv;

    public TransactionCtrl(TransactionResponseSrv transactionResponseSrv) {
        this.transactionResponseSrv = transactionResponseSrv;
    }

    @PostMapping(value = "/upload",  consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response uploadFile(@RequestParam MultipartFile file){
        try {
            TransactionsResponse transactionsResponse = XmlParser.convert(new ByteArrayInputStream(file.getBytes()), TransactionsResponse.class);
            transactionResponseSrv.storeResponse(transactionsResponse);
        } catch (SOAPException | JAXBException | IOException e) {
            e.printStackTrace();
        }
        return Response.ok("Ok");
    }
}
