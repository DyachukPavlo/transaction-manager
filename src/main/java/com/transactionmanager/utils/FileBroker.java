package com.transactionmanager.utils;

import java.io.File;
import java.util.concurrent.ArrayBlockingQueue;

public class FileBroker<T> {
    private ArrayBlockingQueue<File> queue = new ArrayBlockingQueue<File>(5);
    private Class<T> type;
    private FileParams fileParams;
    private boolean produced = false;

    public FileBroker(FileParams fileParams, Class<T> type) {
        this.fileParams = fileParams;
        this.type = type;
    }

    public void put(File data){
        try {
            this.queue.put(data);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public File get(){
        return this.queue.poll();
    }

    public Class<T> getType() {
        return type;
    }

    public FileParams getFileParams() {
        return fileParams;
    }

    public boolean isProduced() {
        return produced;
    }

    public void setProduced(boolean produced) {
        this.produced = produced;
    }

    public ArrayBlockingQueue<File> getQueue() {
        return queue;
    }

    public void setQueue(ArrayBlockingQueue<File> queue) {
        this.queue = queue;
    }
}
