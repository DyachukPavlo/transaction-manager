package com.transactionmanager.utils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.*;

public class XmlParser {
    public static <T> T convert(InputStream inputStream, Class<T> type) throws IOException, JAXBException, SOAPException {
        if (inputStream == null || inputStream.available() == 0){
            return null;
        }
        SOAPMessage message = MessageFactory.newInstance().createMessage(null, inputStream);
        Unmarshaller unmarshaller = JAXBContext.newInstance(type).createUnmarshaller();
        T response = (T) unmarshaller.unmarshal(message.getSOAPBody().extractContentAsDocument());
        return response;
    }
}
