package com.transactionmanager.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Objects;

@Table(schema = "operations", name = "transaction")
@Entity
@XmlRootElement(name = "transaction")
@XmlAccessorType(XmlAccessType.FIELD)
public class Transaction implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "BIGINT")
    private Long id;

    @XmlElement(name = "place")
    @Column(columnDefinition = "VARCHAR(100)", name = "place")
    private String place;

    @XmlElement(name = "amount")
    @Column(columnDefinition = "DOUBLE PRECISION NOT NULL DEFAULT 0", name = "amount")
    private double amount;

    @XmlElement(name = "card")
    @Column(columnDefinition = "VARCHAR(16)", name = "card")
    private String card;

    @XmlElement(name = "currency")
    @Column(columnDefinition = "VARCHAR(5)", name = "currency")
    private String currency;

    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()", name = "transaction_date")
    private Timestamp transactionDate;

    @XmlElement(name = "client")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="client_id", columnDefinition = "BIGINT)")
    private Client client;

    public Transaction() {
        this.transactionDate = Timestamp.valueOf(LocalDateTime.now());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Timestamp getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Timestamp transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Double.compare(that.getAmount(), getAmount()) == 0 &&
                Objects.equals(getId(), that.getId()) &&
                Objects.equals(getPlace(), that.getPlace()) &&
                Objects.equals(getCard(), that.getCard()) &&
                Objects.equals(getCurrency(), that.getCurrency()) &&
                Objects.equals(getTransactionDate(), that.getTransactionDate());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getPlace(), getAmount(), getCard(), getCurrency(), getTransactionDate());
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", place='" + place + '\'' +
                ", amount=" + amount +
                ", card='" + card + '\'' +
                ", currency='" + currency + '\'' +
                ", transactionDate=" + transactionDate +
                ", client=" + client +
                '}';
    }
}
