package com.transactionmanager.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "GetTransactionsResponse", namespace = "http://dbo.qulix.com/ukrsibdbo")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionsResponse {
    @XmlElement(name = "transactions")
    private Transactions transactions;

    public Transactions getTransactions() {
        return transactions;
    }

    public void setTransactions(Transactions transactions) {
        this.transactions = transactions;
    }
}