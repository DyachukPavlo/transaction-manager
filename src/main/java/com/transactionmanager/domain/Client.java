package com.transactionmanager.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Objects;

@Table(schema = "operations", name = "client")
@Entity
@XmlRootElement(name = "client")
@XmlAccessorType(XmlAccessType.FIELD)
public class Client implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "BIGINT")
    private Long id;

    @XmlElement(name = "firstName")
    @Column(columnDefinition = "VARCHAR(50)", name = "first_name")
    private String firstName;

    @XmlElement(name = "lastName")
    @Column(columnDefinition = "VARCHAR(50)", name = "last_name")
    private String lastName;

    @XmlElement(name = "middleName")
    @Column(columnDefinition = "VARCHAR(50)", name = "middle_name")
    private String middleName;

    @XmlElement(name = "inn")
    @Column(columnDefinition = "VARCHAR(10)", name = "inn")
    private String inn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }
}
