package com.transactionmanager.controllers;


import com.transactionmanager.config.Datasource;
import com.transactionmanager.config.JPA;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = {JPA.class, Datasource.class},
        initializers = {ConfigFileApplicationContextInitializer.class})
@RunWith(SpringRunner.class)
@WebMvcTest(TransactionCtrl.class)
public class TransactionCtrlTest {
    private InputStream is;
    private MockMvc mockMvc;

    @MockBean
    private TransactionCtrl transactionCtrl;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp() throws FileNotFoundException {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        File file = new File(Paths.get("/home/webdev/Downloads/transactions/new/transactionsresponse_3.xml").toUri());
        is = new FileInputStream(file);
    }


    @Test
    public void uploadFile() throws Exception {
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", "transactionsresponse_3.xml", MediaType.MULTIPART_FORM_DATA_VALUE, is);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart("/upload").file(mockMultipartFile))
                .andExpect(status().isOk()).andReturn();
        assertEquals(200, result.getResponse().getStatus());
        assertNotNull(result.getResponse().getContentAsString());
    }
}