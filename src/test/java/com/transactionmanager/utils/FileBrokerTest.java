package com.transactionmanager.utils;

import com.transactionmanager.config.Datasource;
import com.transactionmanager.config.JPA;
import com.transactionmanager.domain.TransactionsResponse;
import com.transactionmanager.services.ClientSrv;
import com.transactionmanager.services.TransactionSrv;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.isA;
import static org.junit.Assert.*;

@ContextConfiguration(classes = {JPA.class, Datasource.class, FileParams.class},
        initializers = {ConfigFileApplicationContextInitializer.class})
@RunWith(SpringRunner.class)
public class FileBrokerTest {

    @Autowired
    FileParams fileParams;

    FileBroker<TransactionsResponse> fileBroker;

    @Before
    public void init(){
        fileBroker = new FileBroker<>(fileParams, TransactionsResponse.class);
    }

    @Test
    public void put() {
        fileBroker.put(new File(""));
        assertTrue(fileBroker.getQueue().size()>0);
    }

    @Test
    public void get() {
        fileBroker.put(new File(""));
        assertNotNull(fileBroker.get());
    }

    @Test
    public void getFileParams() {
        assertNotNull(fileParams.getMask());
        assertNotNull(fileParams.getFrom());
        assertNotNull(fileParams.getError());
        assertNotNull(fileParams.getInProcess());
        assertNotNull(fileParams.getProcessed());
    }

    @Test
    public void isProduced() {
        assertFalse(fileBroker.isProduced());
    }

    @Test
    public void setProduced() {
        fileBroker.setProduced(true);
        assertTrue(fileBroker.isProduced());
    }
}