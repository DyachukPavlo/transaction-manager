package com.transactionmanager.utils;

import com.transactionmanager.domain.TransactionsResponse;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.*;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class XmlParserTest {

    @Test
    public void convert_Null_args() throws SOAPException, JAXBException, IOException {
        assertNull(XmlParser.convert(null, null));
    }
    @Test
    public void convert_Empty_file() throws SOAPException, JAXBException, IOException {
        byte[] bytes = new byte[0];
        assertNull(XmlParser.convert(new ByteArrayInputStream(bytes), null));
    }

    @Test
    public void convert_Valid_args() throws SOAPException, JAXBException, IOException {
        File file = new File(Paths.get("/home/webdev/Downloads/transactions/new/transactionsresponse_0.xml").toUri());
        InputStream inputStream = FileUtils.openInputStream(file);
        assertNotNull(XmlParser.convert(inputStream, TransactionsResponse.class));
    }

    @Test
    public void convert_check_instance() throws SOAPException, JAXBException, IOException {
        File file = new File(Paths.get("/home/webdev/Downloads/transactions/new/transactionsresponse_0.xml").toUri());
        InputStream inputStream = FileUtils.openInputStream(file);
        assertTrue(XmlParser.convert(inputStream, TransactionsResponse.class) instanceof TransactionsResponse);
    }

}