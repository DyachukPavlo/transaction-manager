package com.transactionmanager.services;

import com.transactionmanager.config.Datasource;
import com.transactionmanager.config.JPA;
import com.transactionmanager.domain.TransactionsResponse;
import com.transactionmanager.utils.FileBroker;
import com.transactionmanager.utils.FileParams;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileFilter;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

@ContextConfiguration(classes = {JPA.class, Datasource.class, FileParams.class},
        initializers = {ConfigFileApplicationContextInitializer.class})
@RunWith(SpringRunner.class)
public class FileProducerTest {
    @Autowired
    FileParams fileParams;

    FileBroker<TransactionsResponse> fileBroker;
    File[] listOfFiles;

    @Before
    public void init(){
        fileBroker = new FileBroker<>(fileParams, TransactionsResponse.class);
        File folder = new File(Paths.get(fileBroker.getFileParams().getFrom()).toUri());
        FileFilter fileFilter = new WildcardFileFilter(fileBroker.getFileParams().getMask(), IOCase.INSENSITIVE);
        listOfFiles = folder.listFiles(fileFilter);
    }


    @Test
    public void run() throws InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(1);
        FileProducer producer = new FileProducer(fileBroker);
        threadPool.execute(producer);
        threadPool.shutdown();

        Thread.sleep(200);
        assertThat(fileBroker.getQueue().size(), is(listOfFiles.length));
    }
}