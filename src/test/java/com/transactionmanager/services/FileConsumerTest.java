package com.transactionmanager.services;

import com.transactionmanager.config.Datasource;
import com.transactionmanager.config.JPA;
import com.transactionmanager.domain.TransactionsResponse;
import com.transactionmanager.utils.FileBroker;
import com.transactionmanager.utils.FileParams;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileFilter;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.AdditionalMatchers.not;

@ContextConfiguration(classes = {JPA.class, Datasource.class, FileParams.class, TransactionSrv.class, ClientSrv.class},
        initializers = {ConfigFileApplicationContextInitializer.class})
@RunWith(SpringRunner.class)
public class FileConsumerTest {
    @Autowired
    FileParams fileParams;
    @Autowired
    TransactionSrv transactionSrv;
    @Autowired
    ClientSrv clientSrv;

    FileBroker<TransactionsResponse> fileBroker;
    File[] listOfFiles;
    Integer clientsQuantity;
    Integer transactionsQuantity;

    @Before
    public void init(){
        fileBroker = new FileBroker<>(fileParams, TransactionsResponse.class);
        File folder = new File(Paths.get(fileBroker.getFileParams().getFrom()).toUri());
        FileFilter fileFilter = new WildcardFileFilter(fileBroker.getFileParams().getMask(), IOCase.INSENSITIVE);
        listOfFiles = folder.listFiles(fileFilter);
        clientsQuantity = clientSrv.findAll().size();
        transactionsQuantity = transactionSrv.findAll().size();
    }

    @Test
    @Transactional
    @Rollback
    public void run() throws InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(4);
        threadPool.execute(new FileProducer<>(fileBroker));
        boolean isData = fileBroker.getQueue().size() > 0;
        threadPool.execute(new FileConsumer(fileBroker, transactionSrv));
        threadPool.execute(new FileConsumer(fileBroker, transactionSrv));
        threadPool.execute(new FileConsumer(fileBroker, transactionSrv));
        threadPool.shutdown();

        Thread.sleep(10000);
        assertNull(fileBroker.get());

        if (isData){
            assertTrue(clientSrv.findAll().size()>clientsQuantity);
        }

        assertEquals(clientSrv.findAll().size() - clientsQuantity, transactionSrv.findAll().size() - transactionsQuantity);
    }
}